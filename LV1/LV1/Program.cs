﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Program
    {

        static void Main(string[] args)
        {
            Zabiljeska zab1 = new Zabiljeska("bla bla", "Stefan Uglješić", 1);
            Zabiljeska zab2 = new Zabiljeska("kolega", "Matija Vidović");
            Zabiljeska zab3 = new Zabiljeska("Dragutin Tadijanovic");
            Console.WriteLine("zabiljeska 1 : " + zab1.GetText() + " TEXT -" + zab1.GetAutor());
            Console.WriteLine("zabiljeska 2 : " + zab2.GetText() + " TEXT -" + zab2.GetAutor());
            Console.WriteLine("zabiljeska 3 : " + zab3.GetText() + " TEXT -" + zab3.GetAutor());
            Console.WriteLine(zab1.ToString());
            VremenskaZabiljeska zab5 = new VremenskaZabiljeska("bla bla", "Stefan Uglješić", 1);
            Console.WriteLine(zab5.ToString());

            ToDo spisak = new ToDo();
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("-"+i+"-"+"UNESI AUTORA:");
                string autor = Console.ReadLine();
                Console.WriteLine("-"+i+"-"+"UNESI TEKST:");
                string text = Console.ReadLine();
                Console.WriteLine("-" + i + "-" + "UNESI PRIORITET:");
                int prioritet = Convert.ToInt32( Console.ReadLine());
                spisak.DodajZabiljesku(new Zabiljeska(text, autor, prioritet));
            }
            Console.WriteLine(spisak.Ispis());
            int max = 0,indeks=0;
            for(int i=0; i<3;i++)
            {
                if(spisak.DohvatiZabiljesku(i).GetLevelOfImportance()>max)
                {
                    max = spisak.DohvatiZabiljesku(i).GetLevelOfImportance();
                    indeks = i;
                }
            }

            Console.WriteLine("NAKON BRISANJA:");
            spisak.UkloniZabiljesku(indeks);
            Console.WriteLine(spisak.Ispis());
            Console.ReadLine();
        }
    }
}
