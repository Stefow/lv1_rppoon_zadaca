﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class ToDo:IMethods
    {
        private List<Zabiljeska> zabiljeske;
        public ToDo ()
        {
            zabiljeske = new List<Zabiljeska>();
        }
        public void DodajZabiljesku(Zabiljeska zab)
        {
            zabiljeske.Add(zab);
        }
        public void UkloniZabiljesku(int indeks)
        {
            zabiljeske.RemoveAt(indeks);
        }
        public Zabiljeska DohvatiZabiljesku(int indeks)
        {
            return this.zabiljeske[indeks];
        }
        public string Ispis()
        {
            string ispis="";
                foreach (object var in zabiljeske)
                {
                ispis += var.ToString()+"\n";
                }
            return ispis;
        }

    }


     interface IMethods
    {
        void DodajZabiljesku(Zabiljeska zab);
        void UkloniZabiljesku(int indeks);
        Zabiljeska DohvatiZabiljesku(int indeks);
        string Ispis();
    }
}
