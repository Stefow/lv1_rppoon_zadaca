﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class VremenskaZabiljeska: Zabiljeska
    {
        private  DateTime time;
        public VremenskaZabiljeska(DateTime dt, string txt, string au, int loi) : base(txt, au, loi)
        {
            time = dt;
        }
        public VremenskaZabiljeska(string txt, string au, int loi) : base(txt, au, loi)
        {
            time = DateTime.Now;
        }
        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }
        public override string ToString()
        {
            return ("autor - " + base.Autor + " tekst -" + base.Text+" Time -"+time.ToString());
        }
    }
}
