﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Zabiljeska
    {
        private string text;
        private string autor;
        private int levelOfImportance;
        public void SetText(string txt)
        {
            text = txt;
        }
        public void SetLevelOfImportance(int loi)
        {
            levelOfImportance = loi;
        }
        public string GetText()
        {
            return text;
        }
        public string GetAutor()
        {
            return autor;
        }
        public int GetLevelOfImportance()
        {
            return levelOfImportance;
        }
        public string Text
        {
            get{ return this.text; }
            set {this.text=value; }
        }
        public string Autor
        {
            get { return this.autor; }
            private set { this.autor = value; }
        }
        public int LevelOfImportance
        {
            get { return this.levelOfImportance; }
            set { this.levelOfImportance = value; }
        }
        public Zabiljeska(string txt,string au,int loi)
        {
            text = txt;
            autor = au;
            levelOfImportance = loi;
        }
        public Zabiljeska(string txt, string au)
        {
            text = txt;
            autor = au;
            levelOfImportance = 0;
        }
        public Zabiljeska(string au)
        {
            text = "blabla";
            autor = au;
            levelOfImportance = 0;
        }
        public override string ToString()
        {
            return ("autor - "+autor +" tekst -"+ text);
        }

    }

}
